# replix/csi-lib-iscsi package

A simple go package intended to assist CSI plugin authors by providing a tool set to manage iscsi connections.

Based on [github.com/kubernetes-csi/csi-lib-iscsi](https://github.com/kubernetes-csi/csi-lib-iscsi).
Please refer to the documentation there. 

Mind the modifications made for replix. Those are mostly name changes and minor fixes.
